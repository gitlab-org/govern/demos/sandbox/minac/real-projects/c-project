#include <stdio.h>

void copyString(char* dest, const char* src) {
    while (*src != '\0') {
        *dest = *src;
        dest++;
        src++;
    }
}

int main() {
    char buffer[10];
    char* input = "This is a long string that will overflow the buffer";
    copyString(buffer, input);
    printf("Copied string: %s\n", buffer);
    return 0;
}
